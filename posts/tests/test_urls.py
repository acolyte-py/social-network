from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse
from posts.models import Post, Group

User = get_user_model()


class PostURLTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.author = User.objects.create(
            username="jack"
        )

        cls.some_user = User.objects.create(
            username="Тест")

        cls.group = Group.objects.create(
            title="Белый",
            slug="while_slug",
            description="Я белый"
        )

        cls.post = Post.objects.create(
            text="Текст белый",
            pub_date="2021-04-09",
            author=cls.author,
            group=cls.group
        )

    def setUp(self):
        self.guest_client = Client()
        self.authorized_client = Client()
        self.post_author = Client()
        self.post_author.force_login(self.author)
        self.authorized_client.force_login(self.some_user)

    def test_urls_available_to_unauthorized_user(self):
        group_pages = {
            reverse("posts:index"): 200,
            reverse("posts:group_posts", kwargs={"slug": "while_slug"}): 200,
            reverse("posts:new_post"): 302,
            reverse("posts:profile", kwargs={"username": self.author}): 200,
            reverse(
                "posts:post_view",
                kwargs={"username": self.author,
                        "post_id": self.post.id}): 200
        }

        for page, resp in group_pages.items():
            report = self.guest_client.get(page)
            with self.subTest(page=page):
                self.assertEqual(report.status_code, resp,
                                 f"{page} недоступна для "
                                 f"незарегистрированного пользователя"
                                 )

    def test_urls_available_to_authorized_user(self):
        group_pages = {
            reverse("posts:index"): 200,
            reverse("posts:group_posts", kwargs={"slug": "while_slug"}): 200,
            reverse("posts:new_post"): 200,
            reverse("posts:profile", kwargs={"username": self.author}): 200,
            reverse(
                "posts:post_view",
                kwargs={"username": self.author,
                        "post_id": self.post.id}): 200
        }

        for page, resp in group_pages.items():
            with self.subTest(page=page):
                report = self.authorized_client.get(page)
                self.assertEqual(report.status_code, resp,
                                 f"{page} недоступна для "
                                 f"зарегистрированного пользователя"
                                 )

    def test_urls_available_username_post_id_edit_page(self):
        users = {
            self.guest_client: 302,
            self.authorized_client: 302,
            self.post_author: 200
        }

        for user, resp in users.items():
            report = user.get(
                reverse(
                    "posts:post_edit",
                    kwargs={"username": self.author, "post_id": self.post.id}
                )
            )

            self.assertEqual(report.status_code, resp,
                             f"Редактирование недоступно для "
                             f"{user} пользователя"
                             )

    def test_expected_templates_to_urls(self):
        pages_templates = {
            reverse("posts:index"): "index.html",
            reverse("posts:group_posts",
                    kwargs={"slug": "while_slug"}): "group.html",
            reverse("posts:new_post"): "new.html",
        }

        for page, template in pages_templates.items():
            with self.subTest(page=page):
                report = self.authorized_client.get(page)
                self.assertTemplateUsed(report, template,
                                        f"используется некорректный шаблон для"
                                        f" для страницы {page}"
                                        )

    def test_url_post_edit_used_correct_template(self):
        page_templates = {
            reverse(
                "posts:post_edit",
                kwargs={
                    "username": self.author,
                    "post_id": self.post.id}): "post_edit.html"
        }

        for page, template in page_templates.items():
            report = self.post_author.get(page)
            self.assertTemplateUsed(report, template,
                                    f"используется некорректный шаблон для"
                                    f" для страницы {page}"
                                    )

    def test_post_edit_url_used_redirect_for_users_without_access_rights(self):
        redirect_page_for_authorized_client = reverse(
            "posts:post_view",
            kwargs={"username": self.author, "post_id": self.post.id}
        )

        login_page = reverse("login")

        post_edit_page = reverse(
            "posts:post_edit",
            kwargs={"username": self.author, "post_id": self.post.id}
        )

        redirect_page_for_guest_client = f"{login_page}?next={post_edit_page}"
        report_one = self.authorized_client.get(
            reverse(
                "posts:post_edit",
                kwargs={
                    "username": self.author,
                    "post_id": self.post.id}
            ), follow=True
        )

        report_two = self.guest_client.get(
            reverse(
                "posts:post_edit",
                kwargs={
                    "username": self.author,
                    "post_id": self.post.id}
            ), follow=True
        )

        self.assertRedirects(
            report_one,
            redirect_page_for_authorized_client,
            msg_prefix=(
                "Пользователь не был перенаправлен "
                "на нужную страницу."
            )
        )

        self.assertRedirects(
            report_two,
            redirect_page_for_guest_client,
            msg_prefix=(
                "Пользователь не был перенаправлен "
                "на нужную страницу."
            )
        )

    def test_server_return_404_code(self):
        users = [
            self.guest_client,
            self.authorized_client
        ]
        author_page = "/does_not_exist/"

        for user in users:
            report = user.get(author_page)
            with self.subTest(value=user):
                self.assertEqual(
                    report.status_code,
                    404,
                    "Запрашиваемая страница не вернула ожидаемой код 404"
                )
