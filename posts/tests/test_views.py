import shutil
import tempfile
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse
from django import forms
from posts.models import Post, Group, Comment, Follow

User = get_user_model()


class GroupPostTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        settings.MEDIA_ROOT = tempfile.mkdtemp(dir=settings.BASE_DIR)

        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x02\x00'
            b'\x01\x00\x80\x00\x00\x00\x00\x00'
            b'\xFF\xFF\xFF\x21\xF9\x04\x00\x00'
            b'\x00\x00\x00\x2C\x00\x00\x00\x00'
            b'\x02\x00\x01\x00\x00\x02\x02\x0C'
            b'\x0A\x00\x3B'
        )

        uploaded = SimpleUploadedFile(
            name='small.gif',
            content=small_gif,
            content_type='image/gif'
        )

        cls.author = User.objects.create(
            username="Гена"
        )

        cls.unfollow_user = User.objects.create(
            username='unfollow_username',
        )
        cls.user = User.objects.create(
            username='test_name',
        )

        cls.group = Group.objects.create(
            title="Черный",
            slug="black_slug",
            description=("Я черный"
                         )
        )

        cls.group_two = Group.objects.create(
            title="Тест группа",
            slug="test_slug_two",
            description="На странице этой группы не должен появиться пост"
        )

        cls.post = Post.objects.create(
            text="Текст черный",
            pub_date="2021-04-11",
            author=cls.author,
            group=cls.group,
            image=uploaded,
        )

        cls.post_id = cls.post.id

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(settings.MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()

    def setUp(self):
        self.guest_client = Client()
        self.authorized_client = Client()
        self.authorized_client.force_login(GroupPostTest.user)
        self.authorized_unfollow_client = Client()
        self.authorized_unfollow_client.force_login(
            GroupPostTest.unfollow_user
        )
        self.authorized_client.force_login(self.author)

    def test_pages_uses_correct_templates_for_authorized_client(self):
        templates_pages_name = {
            reverse("posts:index"): "index.html",
            reverse(
                "posts:group_posts",
                kwargs={"slug": "black_slug"}
            ): "group.html",
            reverse("posts:new_post"): "new.html",
        }

        for page, template in templates_pages_name.items():
            report = self.authorized_client.get(page)
            with self.subTest(page=page):
                self.assertTemplateUsed(report, template,
                                        f"используется некорректный шаблон для"
                                        f" для страницы {page}"
                                        )

    def test_new_post_form_correct_context(self):
        response = self.authorized_client.get(reverse("posts:new_post"))
        form_fields = {
            "text": forms.fields.CharField,
            "group": forms.fields.ChoiceField,
        }
        for field_name, field_format in form_fields.items():
            with self.subTest(value=field_name):
                form_field = (
                    response.context.get("form").fields.get(field_name)
                )
                self.assertIsInstance(form_field, field_format)

    def test_index_page_show_correct_context(self):
        response = self.authorized_client.get(reverse("posts:index"))

        post_page_context = response.context.get("page")[0]

        self.assertEqual(post_page_context.text, GroupPostTest.post.text)
        self.assertEqual(
            post_page_context.author.username,
            GroupPostTest.author.username
        )
        self.assertEqual(
            post_page_context.group.title,
            GroupPostTest.group.title
        )

    def test_group_page_show_correct_context(self):
        response = self.authorized_client.get(
            reverse("posts:group_posts", kwargs={"slug": "black_slug"})
        )

        group_title = self.group.title
        group_description = self.group.description
        group_slug = self.group.slug

        self.assertEqual(
            response.context.get("group").title,
            group_title
        )
        self.assertEqual(
            response.context.get("group").description,
            group_description
        )
        self.assertEqual(
            response.context.get("group").slug,
            group_slug
        )

    def test_post_is_on_the_main_page(self):
        new_post = self.post

        response = self.authorized_client.get(reverse("posts:index"))
        self.assertEqual(
            response.context.get("page").object_list[0],
            new_post
        )

    def test_post_is_on_the_group_page(self):
        new_post = self.post
        response_one = self.authorized_client.get(
            reverse("posts:group_posts", kwargs={"slug": self.group.slug})
        )

        self.assertEqual(
            response_one.context.get("page").object_list[0],
            new_post
        )

        response_two = self.authorized_client.get(
            reverse("posts:group_posts", kwargs={"slug": "test_slug_two"})
        )

        self.assertNotIn(
            new_post,
            response_two.context.get("page").object_list
        )

    def test_profile_page_show_correct_context(self):
        response = self.authorized_client.get(
            reverse("posts:profile", kwargs={"username": self.author})
        )

        post_author = response.context.get("author").username

        posts = response.context.get("posts")

        self.assertEqual(
            post_author,
            GroupPostTest.author.username
        )

        for post in posts:
            target_post = post.text
            self.assertEqual(
                target_post,
                GroupPostTest.post.text
            )

    def test_post_page_show_correct_context(self):
        response = self.authorized_client.get(
            reverse(
                "posts:post_view",
                kwargs={"username": self.author,
                        "post_id": self.post.id
                        }
            )
        )

        post_author = response.context.get("post").author.username

        post = response.context.get("post")

        self.assertEqual(
            post_author,
            GroupPostTest.author.username
        )

        target_post = post.text
        self.assertEqual(
            target_post,
            GroupPostTest.post.text
        )

    def test_post_edit_page_show_correct_context(self):
        response = self.authorized_client.get(
            reverse(
                "posts:post_edit",
                kwargs={
                    "username": self.author,
                    "post_id": self.post.id,
                }
            )
        )

        form_fields = {
            "text": forms.fields.CharField,
            "group": forms.fields.ChoiceField,
        }

        for field_name, field_format in form_fields.items():
            with self.subTest(value=field_name):
                form_field = (
                    response.context.get("form").fields.get(field_name)
                )
                self.assertIsInstance(form_field, field_format)

    def test_index_page_contains_given_number_of_posts(self):
        response = self.authorized_client.get(reverse("posts:index"))

        num_posts_per_page = len(response.context.get("page").object_list)

        expected_number_of_posts = 11

        self.assertLess(num_posts_per_page, expected_number_of_posts)

    def test_index_page_context_contains_picture(self):
        response = self.authorized_client.get(reverse("posts:index"))

        picture = response.context.get("page").object_list[0].image

        self.assertEqual(
            picture,
            GroupPostTest.post.image,
            "Контекст главной странице не содержит изображения.")

    def test_profile_page_context_contains_picture(self):
        response = self.authorized_client.get(
            reverse("posts:profile", kwargs={"username": self.author})
        )

        picture = response.context.get("posts")[0].image

        self.assertEqual(
            picture,
            GroupPostTest.post.image,
            "Контекст страницы профиля автора не содержит изображения.")

    def test_group_page_context_contains_picture(self):
        response = self.authorized_client.get(
            reverse("posts:group_posts", kwargs={"slug": self.group.slug})
        )

        picture = response.context.get("page").object_list[0].image

        self.assertEqual(
            picture,
            GroupPostTest.post.image,
            "Контекст страницы группы не содержит изображения.")

    def test_post_page_context_contains_picture(self):
        response = self.authorized_client.get(
            reverse("posts:post_view",
                    kwargs={
                        "username": self.author,
                        "post_id": self.post.id
                    }
                    )
        )

        picture = response.context.get("post").image

        self.assertEqual(
            picture,
            GroupPostTest.post.image,
            "Контекст страницы отдельного поста не содержит изображения.")

    def test_index_caching_page(self):
        response = self.authorized_client.get(reverse("posts:index"))
        cached_response_content = response.content

        Post.objects.create(
            text="Проверка кэша",
            author=self.author,
        )

        response = self.authorized_client.get(reverse("posts:index"))
        cached_response_content_two = response.content

        self.assertEqual(cached_response_content, cached_response_content_two)

    def test_authorize_user_follow_other_users(self):
        followers_before = Follow.objects.filter(
            author=GroupPostTest.author).count()

        self.authorized_client.post(
            reverse('posts:profile_follow',
                    kwargs={'username': GroupPostTest.author}))

        followers_after = Follow.objects.filter(
            author=GroupPostTest.author).count()

        self.assertEqual(followers_before, followers_after)

    def test_authorize_user_unfollow_other_users(self):
        followers_before = Follow.objects.filter(
            author=GroupPostTest.author).count()

        self.authorized_client.post(
            reverse('posts:profile_unfollow',
                    kwargs={'username': GroupPostTest.author}))

        followers_after_delete = Follow.objects.filter(
            author=GroupPostTest.author).count()

        self.assertEqual(followers_before, followers_after_delete)

    def test_new_post_author_add_followers(self):

        Follow.objects.create(
            user=GroupPostTest.user,
            author=GroupPostTest.author
        )

        response_unfollow = self.authorized_unfollow_client.get(
            reverse('posts:follow_index')
        )

        page_unfollow_user_before = response_unfollow.content

        response_follower = self.authorized_client.get(
            reverse('posts:follow_index')
        )

        page_before_new_post = len(response_follower.context.get('page'))

        Post.objects.create(
            text='Новый пост от автора',
            author=GroupPostTest.author
        )

        response_follower_check = self.authorized_client.get(
            reverse('posts:follow_index')
        )

        page_after_new_post = len(response_follower_check.context.get('page'))

        response_unfollow = self.authorized_unfollow_client.get(
            reverse('posts:follow_index')
        )

        page_unfollow_user_after = response_unfollow.content

        self.assertEqual(page_before_new_post, page_after_new_post)

        self.assertEqual(page_unfollow_user_before, page_unfollow_user_after)

    def test_only_an_authorized_user_can_comment_on_posts(self):
        form_data = {
            'text': 'test_text',
            'author': GroupPostTest.user,
            'post': GroupPostTest.post_id,
        }

        comments_count_before = Comment.objects.count()

        self.authorized_client.post(
            reverse('posts:add_comment', kwargs={
                'username': GroupPostTest.post.author.username,
                'post_id': GroupPostTest.post.id
            }), data=form_data, follow=True,
        )

        self.guest_client.post(
            reverse('posts:add_comment', kwargs={
                'username': GroupPostTest.post.author.username,
                'post_id': GroupPostTest.post.id
            }), data=form_data, follow=True,
        )

        comments_count_after = Comment.objects.count()

        self.assertEqual(comments_count_before + 1, comments_count_after)
