import shutil
import tempfile
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from posts.models import Post, Group

User = get_user_model()


class PostFormTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        settings.MEDIA_ROOT = tempfile.mkdtemp(dir=settings.BASE_DIR)

        small_gif = (
            b'\x47\x49\x46\x38\x39\x61\x02\x00'
            b'\x01\x00\x80\x00\x00\x00\x00\x00'
            b'\xFF\xFF\xFF\x21\xF9\x04\x00\x00'
            b'\x00\x00\x00\x2C\x00\x00\x00\x00'
            b'\x02\x00\x01\x00\x00\x02\x02\x0C'
            b'\x0A\x00\x3B'
        )

        uploaded = SimpleUploadedFile(
            name='small.gif',
            content=small_gif,
            content_type='image/gif'
        )

        cls.author = User.objects.create(
            username="Вася"
        )

        cls.group = Group.objects.create(
            title="Розовый",
            slug="pink_slug",
            description="Я розовый"
        )

        cls.post = Post.objects.create(
            text="Текст розовый",
            pub_date="2021-04-11",
            author=cls.author,
            group=cls.group,
            image=uploaded,
        )

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(settings.MEDIA_ROOT, ignore_errors=True)
        super().tearDownClass()

    def setUp(self):
        self.authorized_client = Client()
        self.authorized_client.force_login(self.author)

    def test_entry_added(self):
        posts_count = Post.objects.count()

        form_data = {
            "group": PostFormTest.group.id,
            "text": "Текст для теста",
            "image": PostFormTest.post.image,
        }

        response = self.authorized_client.post(
            reverse("posts:new_post"),
            data=form_data,
            follow=True
        )

        self.assertEqual(
            Post.objects.count(), posts_count + 1,
            "Запись не была добавлена на сайт"
        )

        self.assertTrue(
            Post.objects.filter(group=PostFormTest.group.id).exists()
        )

        self.assertRedirects(
            response, reverse("posts:index"), status_code=302,
            target_status_code=200,
            fetch_redirect_response=True
        )

    def test_post_changed_in_the_database_when_editing_through_edit_page(self):
        form_data = {
            "group": self.group.id,
            "text": "Измененный пост",
        }
        response = self.authorized_client.post(
            reverse("posts:post_edit", kwargs={"username": self.author,
                                               "post_id": self.post.id}),
            data=form_data,
            follow=True
        )

        self.post.refresh_from_db()

        self.assertRedirects(
            response,
            reverse(
                "posts:post_view",
                kwargs={
                    "username": self.author.username,
                    "post_id": self.post.id
                }
            )
        )

        self.assertEqual(self.post.text, form_data["text"])
