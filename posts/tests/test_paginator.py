import time
from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from posts.models import Post, Group


User = get_user_model()


class PaginatorViewsTest(TestCase):

    @classmethod
    def setUpClass(cls):

        super().setUpClass()
        cls.author = User.objects.create(
            username="Гриша"
        )

        cls.group = Group.objects.create(
            title="Желтый",
            slug="yellow_slug",
            description="Я желтый"
        )

        for post_number in range(1, 14):
            Post.objects.create(
                id=post_number,
                text=(f"Тестовый текст № {post_number}"),
                author=cls.author,
                group=Group(id=1),
            )
            time.sleep(2.0)

    def setUp(self):
        self.guest_client = Client()
        self.authorized_client = Client()
        self.authorized_client.force_login(self.author)

    def test_first_page_contain_ten_records(self):
        response = self.guest_client.get(
            reverse("posts:index"))
        self.assertEqual(len(response.context.get("page").object_list), 10)

    def test_second_page_contain_three_records(self):
        response = self.guest_client.get(
            reverse("posts:index") + "?page=2")
        self.assertEqual(len(response.context.get("page").object_list), 3)
