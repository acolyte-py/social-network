from django.contrib.auth import get_user_model
from django.test import TestCase
from posts.models import Post, Group


User = get_user_model()


class PostModelTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.author = User.objects.create(
            username="Миша"
        )

        cls.group = Group.objects.create(
            title="Красный",
            slug="red_group",
            description="Я красный",
        )

        cls.post = Post.objects.create(
            text="Текст красный",
            pub_date="2021-04-09",
            author=cls.author,
            group=cls.group,
        )

    def test_models_verbose_name(self):
        field_verbose = {
            "text": "Текст поста",
            "group": "Группа",
        }

        for value, expected in field_verbose.items():
            with self.subTest(value=value):
                self.assertEqual(self.post._meta.get_field(value).verbose_name,
                                 expected
                                 )

    def test_models_help_text(self):
        help_text = {
            "text": "Напиши о чем думаешь",
            "group": "Выбери группу",
        }

        for value, expected in help_text.items():
            with self.subTest(value=value):
                self.assertEqual(self.post._meta.get_field(value).help_text,
                                 expected
                                 )

    def test_models_str_method_for_post_text(self):
        post = PostModelTest.post
        expected_object_text = post.text
        self.assertEqual(expected_object_text, str(post),
                         "Текст постов отображается не верно")

    def test_models_str_method_for_group_title(self):
        group = PostModelTest.group
        expected_object_title = group.title
        self.assertEqual(expected_object_title, str(group),
                         "Название группы отображается не верно"
                         )
