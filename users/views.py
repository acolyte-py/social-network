from django.views.generic import CreateView

from django.urls import reverse_lazy

from .forms import CreationForm

from django.contrib.auth.decorators import login_required


class SignUp(CreateView):
    form_class = CreationForm
    success_url = reverse_lazy("signup")
    template_name = "signup.html"


@login_required
def my_view(request):
    ...
